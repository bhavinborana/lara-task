@extends('layout.main')

@section('content')
    <section class="content-header">
        <h1>
            Surveys
        </h1>
   </section>
   <div class="content">
       @include('layout.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($surveys, ['route' => ['surveys.update', $surveys->id], 'method' => 'patch']) !!}

                        @include('surveys.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

