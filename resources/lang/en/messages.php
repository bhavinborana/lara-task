<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Static Text Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'users_add' => 'User saved successfully',
    'users_not_found' =>  'User not found',
    'users_updated' => 'User updated successfully.'

];
