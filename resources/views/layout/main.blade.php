<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Laravel | Admin Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('AdminLTE/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Select 2 -->
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/select2/select2.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/AdminLTE.min.css')}}">
    
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/skins/skin-blue.min.css')}}">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- jQuery 2.1.4 -->
    <script src="{{asset('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/jQueryUI/jquery-ui.min.js')}}"></script>
    
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/fastclick/fastclick.js')}}"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    @yield('styles')

    @yield('plugins')
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    <div id="bgScreen" style="z-index: 9999;width: 100%;height: 100%;background: rgba(10,10,10,0.8);position: fixed;top: 0;left: 0; display:none">
      <img src="{{ asset('images/loading.gif') }}" id="gif" style="position: fixed;
      top: 30%;
      left: 45%;
      width: 100px;
      height: 100px;
      background-repeat: no-repeat;
      background-position: center;
      z-index: 1000;
      -moz-opacity: 0.8;
      opacity: .70;
      filter: alpha(opacity=80);
      display: block;
  visibility: hidden;">

    </div>

     @include('layout.header')

     @include('layout.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
            
            @yield('content')

      </div><!-- /.content-wrapper -->

      @include('layout.footer')

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>

      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <!-- AdminLTE App -->
    <script src="{{asset('AdminLTE/dist/js/app.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript">
      $(function() {
             var arr = location.pathname.split("/");
             var pattern  = arr[1];
            $('.sidebar-menu  li a[href*="/' + pattern + '"]').closest('li').addClass('active');
        });
    </script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->

      @yield('scripts')
  </body>
</html>
