<?php
/*
 * File: UsersController.php
 * Author: Bharat Borana 
 * CreatedOn: date (15/Dec/2018) 
*/

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UpdateSurveyRequest;
use App\Http\Requests\AddSurveyRequest;
use App\Http\Controllers\Controller;
use DataTables;
use Laracasts\Flash\Flash;

use App\User;
use App\Models\Surveys;
use Lang;


class SurveysController extends Controller
{
    /** @var  data */
    protected $data;
    

    public function __construct()
    {
        
    }
    
    /**
     * Display a listing of the teams.
     *
     * @return Response
     */
    public function index()
    { 
        return view('surveys.index');
    }

    /**
     * Show the form for creating a new teams.
     *
     * @return Response
     */
    public function create()
    {
        $users = User::select('name', 'id')->get()->toArray(); 
        $users = array_pluck($users,  'name', 'id');

        return view('surveys.create')->with('users',$users);
    }

    /**
     * Store a newly created teams in storage.
     *
     * @param CreateTeamsRequest $request
     *
     * @return Response
     */
    public function store(AddSurveyRequest $request)
    {
        
        $input = $request->all();
        $surveys = Surveys::create($input);

        Flash::success(Lang::get('messages.users_add'));

        return redirect(route('surveys.index'));
    }

    /**
     * Display the specified teams.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $surveys = Surveys::select('id','name','user_id')->with(['users' => function($user){ $user->select('id','name'); }])->where('id',$id)->first($id);
        if (empty($surveys)) {
            Flash::error(Lang::get('messages.users_not_found'));

            return redirect(route('surveys.index'));
        }
        return view('surveys.show')->with('surveys', $surveys);
    }

    /**
     * Show the form for editing the specified teams.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $surveys = Surveys::select('id','name','user_id')->findOrFail($id);
        $users = User::select('name', 'id')->get()->toArray(); 
        $users = array_pluck($users,  'name', 'id');

        if (empty($surveys)) {
            Flash::error(Lang::get('messages.users_not_found'));

            return redirect(route('surveys.index'));
        }

        return view('surveys.edit')->with('surveys', $surveys)->with('users',$users);
    }

    /**
     * Update the specified teams in storage.
     *
     * @param  int              $id
     * @param UpdateteamsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSurveyRequest $request)
    {
        $surveys = Surveys::findOrFail($id);
        if (empty($surveys)) {
            Flash::error(Lang::get('messages.users_not_found'));

            return redirect(route('surveys.index'));
        }
        $surveys = $surveys->update($request->all(), [$id]);

        Flash::success(Lang::get('messages.users_updated'));

        return redirect(route('surveys.index'));
    }

    /**
     * Remove the specified teams from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $surveys = Surveys::findOrFail($id);

        if (empty($surveys)) {
            Flash::error(Lang::get('messages.users_not_found'));

            return redirect(route('surveys.index'));
        }

        $surveys->delete($id);

        Flash::success(Lang::get('messages.users_updated'));

        return redirect(route('surveys.index'));
    }
    
    /**
     * Show Datatable Grid
     */
    public function getGrid()
    {
        $results = Surveys::select('id','name','user_id')->with(['users' => function($user){ $user->select('id','name'); }])->get();
                 
        return DataTables::of($results)
                ->addColumn('action', 'surveys.datatables_actions')
                ->addColumn('user_name', function ($survey) {
                    if($survey->users->name){
                        return $survey->users->name;
                    }
                    else{
                        return "";
                    }
                })
                ->make(TRUE);               
    }
    
    

}
