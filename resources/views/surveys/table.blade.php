<table id="surveys" class="table table-condensed">
    <thead>
        <tr>
            <th>Survey Name</th>
            <th>User Name</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

@section('scripts')
  
    <script>
        $(function () {
            $('#surveys').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                "autoWidth": false,
                ajax: '{{ url("surveys/grid") }}',
                "columnDefs": [ {

                "targets": [2], // column or columns numbers

                "orderable": false,  // set orderable for selected columns


                }],
                "order": [[ 0, "desc" ]],
                "aoColumns": [
                    {"data":"name","name":"name"},
                    {"data":"user_name","name":"user_name"},
                    {"data":"action","name":"action"}
                ]
            });
        });
    </script>
@endsection