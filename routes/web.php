<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', function () {
      return redirect('users/index');
    });

    /* Adding users Route */
    Route::get('users/grid', 'UsersController@getGrid');
    Route::resource('users', 'UsersController');

    /* Adding survey Route */
    Route::get('surveys/grid', 'SurveysController@getGrid');
    Route::resource('surveys', 'SurveysController');