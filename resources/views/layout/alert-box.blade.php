 <div class="modal modal-primary" id="alertModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Info Box</h4>
        </div>
        <div class="modal-body">
          <p class="msg"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-right ok" >Ok</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
 </div>