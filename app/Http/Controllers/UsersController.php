<?php
/*
 * File: UsersController.php
 * Author: Bharat Borana 
 * CreatedOn: date (15/Dec/2018) 
*/

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Controllers\Controller;
use DataTables;
use Laracasts\Flash\Flash;

use App\Models\User;
use Lang;


class UsersController extends Controller
{
    /** @var  data */
    protected $data;
    

    public function __construct()
    {
        
    }
    
    /**
     * Display a listing of the teams.
     *
     * @return Response
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new teams.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create',$this->data);
    }

    /**
     * Store a newly created teams in storage.
     *
     * @param CreateTeamsRequest $request
     *
     * @return Response
     */
    public function store(CreateTeamsRequest $request)
    {
        
        $input = $request->all();

        $teams = User::create($input);

        Flash::success(Lang::get('messages.users_add'));

        return redirect(route('users.index'));
    }

    /**
     * Display the specified teams.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $users = User::select('id','name','email')
                ->findOrFail($id);
        if (empty($users)) {
            Flash::error(Lang::get('messages.users_not_found'));

            return redirect(route('users.index'));
        }
        return view('users.show')->with('users', $users);
    }

    /**
     * Show the form for editing the specified teams.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $users = User::select('id','name','email')
                ->findOrFail($id);
        if (empty($users)) {
            Flash::error(Lang::get('messages.users_not_found'));

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('users', $users);
    }

    /**
     * Update the specified teams in storage.
     *
     * @param  int              $id
     * @param UpdateteamsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $teams = User::findOrFail($id);
        if (empty($teams)) {
            Flash::error(Lang::get('messages.users_not_found'));

            return redirect(route('users.index'));
        }
        $teams = $teams->update($request->all(), [$id]);

        Flash::success(Lang::get('messages.users_updated'));

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified teams from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $teams = User::findOrFail($id);

        if (empty($teams)) {
            Flash::error(Lang::get('messages.users_not_found'));

            return redirect(route('users.index'));
        }

        $teams->delete($id);

        Flash::success(Lang::get('messages.users_updated'));

        return redirect(route('users.index'));
    }
    
    /**
     * Show Datatable Grid
     */
    public function getGrid()
    {
        $results = User::select('id','name','email')->get();
                 
        return DataTables::of($results)
                ->addColumn('action', 'users.datatables_actions')
                ->make(TRUE);               
    }
    
    

}
