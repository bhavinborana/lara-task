<table id="users" class="table table-condensed">
    <thead>
        <tr>
            <th>User Name</th>
            <th>User Email</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

@section('scripts')
  
    <script>
        $(function () {
            $('#users').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                "autoWidth": false,
                ajax: '{{ url("users/grid") }}',
                "columnDefs": [ {

                "targets": [2], // column or columns numbers

                "orderable": false,  // set orderable for selected columns


                }],
                "order": [[ 0, "desc" ]],
                "aoColumns": [
                    {"data":"name","name":"name"},
                    {"data":"email","name":"email"},
                    {"data":"action","name":"action"}
                ]
            });
        });
    </script>
@endsection