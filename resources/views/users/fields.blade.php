<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 25]) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control','maxlength' => 25,'type' => 'email']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure to save this User?')"]) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
</div>


@section('scripts')
<script>
    
</script>
@endsection