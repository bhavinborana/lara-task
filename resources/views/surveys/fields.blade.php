<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Survey Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 25]) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Name:') !!}
    {!! Form::select('user_id', $users, @$surveys->user_id, ['class' => 'form-control select2','maxlength' => 25]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure to save this Survey?')"]) !!}
    <a href="{!! route('surveys.index') !!}" class="btn btn-default">Cancel</a>
</div>


@section('scripts')
<script>
    
</script>
@endsection