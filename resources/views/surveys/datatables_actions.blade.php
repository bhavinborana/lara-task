{!! Form::open(['route' => ['surveys.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('surveys.show', $id) }}" class='btn btn-default btn-xs' title="View">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('surveys.edit', $id) }}" class='btn btn-default btn-xs' title="Edit">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        "title"=> "Delete",
        'onclick' => "return confirm('Are you sure to remove these Survey?')"
    ]) !!}
</div>
{!! Form::close() !!}
