<div class="modal modal-primary" id="deleteConfirmModal">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Alert</h4>
       </div>
       <div class="modal-body">
         <p class="msg">Do you want to Delete this company ?</p>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-outline pull-right no" data-dismiss="modal">No</button>
         <button type="button" class="btn btn-outline pull-right yes" style="margin-right:10px;" >Yes</button>
       </div>
     </div>
     <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
